/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.*;
/**
 *
 * @author Johan
 */
public class Conexion {
        private String username = "root";
    private String password = "";
    private String host = "localhost";
    private String port = "3306";
    private String db = "bd_web";
    private String className = "com.mysql.jdbc.Driver";
    private String url = "jdbc:mysql://"+host+":"+port+"/"+db;
    private Connection con;
    
    public Conexion(){
        try{
            Class.forName(className);
            con = DriverManager.getConnection(url, username, password);
           
    }
        catch(ClassNotFoundException e){
            System.out.println(e);
        }
        catch(SQLException e){
            System.out.println(e);
        }
    }
    public Connection getConnection(){
        return con;
    }
    
  
}
