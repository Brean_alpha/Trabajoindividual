/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import java.sql.*;
/**
 *
 * @author Johan
 */
public class ConsultasSql extends Conexion{
    
    public boolean login (String user, String pass){
    
        PreparedStatement pst = null;
        ResultSet rs = null;
        
        try{
        
        String consulta ="Select * from usuario where nickUsuario = ? and contraseñaUsuario = ? ";
        pst = getConnection().prepareStatement(consulta);
        pst.setString(1, user);
        pst.setString(2, pass);
        rs = pst.executeQuery();
        if(rs.absolute(1)){
        
            return true;
        
        }
        }catch(SQLException e){
        
            System.err.println("Error"+e);
            return false;
        }  finally{
            
                
            
            try{
                if (getConnection()!=null){
                    getConnection().close();
                if(pst!=null) pst.close();
                if (rs!=null) rs.close();
                }
            }
            catch(Exception e){
                System.out.println(e);
            }
        }
        
       return false; 
    
    }
    
    public boolean Registro (String rut,String nombre, String apellido, String telefono, String email, int idCiudad){
    
        PreparedStatement pst = null;
     
        try{
        
String consulta = "Insert into cliente(rutCliente,nombreCliente,apellidoCliente,fonoCliente,emailCliente,idCiudad) values(?,?,?,?,?,?)";
        
      pst = getConnection().prepareStatement(consulta);
           pst.setString(1, rut);
           pst.setString(2, nombre);
           pst.setString(3, apellido);
           pst.setString(4, telefono);
           pst.setString(5, email);
           pst.setInt(6, idCiudad);
         
          if(pst.executeUpdate()==1){
          return true;
          
          }
        }catch(SQLException e){
        
            System.err.println("Error"+ e);
                return false;
        }
          finally{
            
                
            
            try{
                if (getConnection()!=null){
                    getConnection().close();
                if(pst!=null) pst.close();
               
                }
            }
            catch(Exception e){
                System.out.println(e);
            }
        }
    
    return false;
    }
    
    public boolean eliminar ( String rut){
    
            PreparedStatement pst = null;
     
        try{
        
String consulta = "delete from cliente where rutCliente = ?";
        
      pst = getConnection().prepareStatement(consulta);
           pst.setString(1,rut);           
          if(pst.executeUpdate()==1){
          return true;
          
          }
        }catch(SQLException e){
        
            System.err.println("Error"+ e);
                return false;
        }
          finally{
            
                
            
            try{
                if (getConnection()!=null){
                    getConnection().close();
                if(pst!=null) pst.close();
              
                }
            }
            catch(Exception e){
                System.out.println(e);
            }
        }
        return false;
    }
    
    public boolean editar( String nombre, String apellido, String telefono, String email, int idCiudad, String rut ){

        PreparedStatement pst =null;
try{

    String consulta = "UPDATE cliente SET nombreCliente= ?,apellidoCliente= ?,fonoCliente=?,emailCliente=?,idCiudad=? WHERE rutCliente = ?";

 pst = getConnection().prepareStatement(consulta);
           pst.setString(1, nombre);
           pst.setString(2, apellido);
           pst.setString(3, telefono);
           pst.setString(4, email);
           pst.setInt(5, idCiudad);
           pst.setString(6, rut);
         
          if(pst.executeUpdate()==1){
          return true;
          
          }
        }catch(SQLException e){
        
            System.err.println("Error"+ e);
                return false;
        }
          finally{
            
                
            
            try{
                if (getConnection()!=null){
                    getConnection().close();
                if(pst!=null) pst.close();
               
                }
            }
            catch(Exception e){
                System.out.println(e);
            }
        }
        return false;
}



  
    
}
