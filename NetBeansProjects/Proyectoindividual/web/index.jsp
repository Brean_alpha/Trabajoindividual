<%-- 
    Document   : index
    Created on : 28-ago-2018, 0:57:37
    Author     : Johan
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="Modelo.ConsultasSql"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body>
           <nav class="navbar navbar-dark text-white" style="background-color: #5C085C;">
              <a class="navbar-brand">Bienvenido seas :)</a>
              <form class="form-inline">
                  <a href ="Login.jsp" class="btn btn-lg mb-2 text-white" style="background-color: #5C085C;border:  3px solid white;">Login</a>
              </form>
            </nav>
        
        <label></label>
        <div class="container-fluid center" style="align-content: center; padding: 500px; padding-top: 50px;">
            <h1 class="h3 mb-3 text-left" style="color:#5C085C;"> Clientes Registrados:</h1>
        <table  class="table table-dark mt-3 text-white" style="background-color: #5C085C;">
                      <thead>
                        <tr>                      
                          <th scope="col">Nombre</th>
                          <th scope="col">Apellido</th>
                        
        
                        </tr>
                      </thead>
                      <tbody>         
                                      <% 
                    ConsultasSql c = new ConsultasSql();
                    PreparedStatement pst = null;
                    ResultSet rs = null;
                    pst= c.getConnection().prepareStatement("Select nombreCliente, apellidoCliente from cliente");
                        rs = pst.executeQuery();
                                while(rs.next()){
                            out.println("<tr><td>"+rs.getString(1)+"</td>");
                            out.println("<td>"+rs.getString(2)+"</td></tr>");
                          
                        }
                              
                              %>
                      </tbody>
            </table>
        </div>
    </body>
    <footer class="page-footer text-white" style="background-color:#5C085C; ">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2018 Copyright:
    <a> Registros.com</a>
  </div>
  <!-- Copyright -->

</footer>
</html>
