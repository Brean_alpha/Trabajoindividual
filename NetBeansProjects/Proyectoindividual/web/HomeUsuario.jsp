<%-- 
    Document   : HomeUsuario
    Created on : 28-ago-2018, 2:04:25
    Author     : Johan
--%>

<%@page import="Modelo.ConsultasSql"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
           <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
       
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Usuario</title>
    </head>
    <body>
        <nav class="navbar navbar-dark text-white" style="background-color: #5C085C;">
              <a class="navbar-brand">Home Usuario</a>
              <form class="form-inline">
                  <a href ="index.jsp" class="btn btn-lg mb-2 text-white" style="background-color: #5C085C;border:  3px solid white;">Cerrar sesion</a>
              </form>
            </nav>
        
        
         <div class="container-fluid center" style="align-content: center; padding: 200px; padding-top: 50px;">
            <h1 class="h3 mb-3 text-left" style="color:#5C085C;"> Ingreso Clientes</h1>
        <table  class="table table-dark mt-3 text-white tabla-post" style="background-color: #5C085C;">
                      <thead>
                        <tr>
                         
                          <th scope="col">Rut</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Apellido</th>
                          <th scope="col">Telefono</th>
                          <th scope="col">Email</th>
                          <th scope="col">Ciudad</th>
                          
                        </tr>
                      </thead>
                      <tbody>          
                                  <% 
                    ConsultasSql c = new ConsultasSql();
                    PreparedStatement pst = null;
                    ResultSet rs = null;
                    pst= c.getConnection().prepareStatement("Select rutCliente,nombreCliente,apellidoCliente,fonoCliente,emailCliente,nombreCiudad from cliente inner join ciudad on cliente.idCiudad = ciudad.idCiudad");
                        rs = pst.executeQuery();
                            
                
                        
                        
                                while(rs.next()){
                            out.println("<tr><td>"+rs.getString(1)+"</td>");
                            out.println("<td>"+rs.getString(2)+"</td>");
                            out.println("<td>"+rs.getString(3)+"</td>");
                            out.println("<td>"+rs.getString(4)+"</td>");
                            out.println("<td>"+rs.getString(5)+"</td>");
                            out.println("<td>"+rs.getString(6)+"</td></tr>");
                            
                            
                            
                        }
                                
                    
                        
                       
                        
                        
                                

                               
                                
                              %>
                      </tbody>
            </table>
    <a  href="Registro.jsp" style="background-color: #00cc00 ;border:  3px solid white;" class="btn   text-white rounded-2 mb-3"  > Ingresar cliente +</a>
    <a  href="Editar.jsp" style="background-color:  #6666ff ;border:  3px solid white;" class="btn   text-white rounded-2 mb-3"  > Editar cliente #</a>
   <form method="GET" action="eliminar" >              
      <div class="input-group mb-3" style="padding-right: 400px;">
           <div class="input-group-prepend">
               <button class="btn btn-outline text-white" style="background-color:  #cc0000" type="submit">Eliminar Cliente -</button>
              </div>
          <input name="rutcliente" class="form-control" required="" placeholder="ingrese rut cliente a eliminar">
      
     
     </div>
  </form> 
    
    
    
  
    
    </body>
   
</html>
