<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelo.ConsultasSql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%-- 
    Document   : Registro
    Created on : 28-ago-2018, 19:58:06
    Author     : Johan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
       
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro</title>
    </head>
    <body>
         <nav class="navbar navbar-dark text-white" style="background-color: #5C085C;">
              <a class="navbar-brand">Registro</a>
              <form class="form-inline">
                  <a href ="HomeUsuario.jsp" class="btn btn-lg mb-2 text-white" style="background-color: #5C085C;border:  3px solid white;">Home usuario</a>
              </form>
            </nav>
        
          <div class="container" style=" align-content: center; padding: 400px; padding-top: 100px;">
            <form method="GET" action="registro">
                <h1 class="text-center"> Ingrese su datos</h1><br>
               
                <div class="group">
                    <input name="rut" class="form-control" required=""  placeholder="ingrese rut"><br>
                <input name="nombre" class="form-control" required="" placeholder="ingrese nombre"><br>
                <input name="apellido" class="form-control" required="" placeholder="ingrese apellido"><br>
                <input name="fono" class="form-control" required="" placeholder="ingrese telefono"><br>
                <input name="email" class="form-control" required="" placeholder="ingrese Email"><br>
                
             
                
                <select name="ciudad" class="custom-select custom-select-light mb-3" required="">
                             <option selected disabled>Ciudad</option>
                                 <%
                       ConsultasSql c = new ConsultasSql();
                    ArrayList al = null;       
                    PreparedStatement  pst= null;
                    ResultSet  rs= null;
                    pst= c.getConnection().prepareStatement("Select * from ciudad");
                    rs = pst.executeQuery();
                        while(rs.next()){
                               al = new ArrayList();
                            out.println("<option id='ciudad"+rs.getString(1)+"'value='"+rs.getString(1)+"'>"+rs.getString(2)+"</option>");
                        }
                    %>
                              </select><br>
                
                
                
                </div>
           <button type="submit" class="btn btn-lg  text-white rounded-1 mb-3" style="background-color: #5C085C;border:  3px solid white;">Registrar Cliente</button>
            </form>
            </div>
  
        
    </body>
        <footer class="page-footer text-white" style="background-color:#5C085C; ">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2018 Copyright:
    <a> Registros.com</a>
  </div>
  <!-- Copyright -->

</footer>
</html>
